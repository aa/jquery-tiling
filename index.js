$(document).ready(function() {
    // Initialize the plugin
    $("div#wrapper").tiling({
        'layouts' : [
            'horizontal',
            'vertical',
            'fair',
            'test',
        ], 
        'selector' : 'div.box',
        // 'post_init' : function() {
        //     // console.log('triggered!');
        // },
    });
    $("div#wrapper").tiling("tile_horizontally");

    // Binds keys to change the layout
    $(window).keypress(function(event) {
        // Press F to adapt the font size
        if (event.which == '102') {
            event.preventDefault();
            $("div#wrapper").tiling("adapt_fontsize");
        }
        // Press N to go to the next layout
        if (event.which == '110') {
            event.preventDefault();
            $("div#wrapper").tiling("next");
        }
        // Press P to go to the previous layout
        if (event.which == '112') {
            event.preventDefault();
            $("div#wrapper").tiling("previous");
        }
    });

    $(window).resize(function() {
        $("div#wrapper").tiling("update");
    });
});
